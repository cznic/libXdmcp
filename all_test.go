// Copyright 2024 The libXdmcp-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libxdmcp // import "modernc.org/libxdmcp"

import (
	"fmt"
	"os"
	"runtime"
	"testing"

	_ "modernc.org/cc/v4"       // generator.go
	_ "modernc.org/ccgo/v4/lib" // generator.go
	"modernc.org/fileutil/ccgo" // generator.go
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	target = fmt.Sprintf("%s/%s", goos, goarch)
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func Test(t *testing.T) {
	switch target {
	case "darwin/arm64", "freebsd/amd64":
		t.Skip()
	}

	if _, err := ccgo.Shell(nil, "go", "run", "./internal/test"); err != nil {
		t.Fatal(err)
	}
}
